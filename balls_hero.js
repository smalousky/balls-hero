function BallsHero(options) {
    var scope = this;
    this.options = options;

    this.canvas = options['canvas'];
    var ctx = this.canvas.getContext('2d');
    this.canvas.addEventListener('click', on_click);

    var images = options['images'];
    var w=100, h=100;

    this.render = function () {

        clear();
        this.draw_all();
    };


    function rand_image() {
        var l = images.length-1;
        return images[Math.round(Math.random()*l)];
    }


    function on_resize() {

        w = scope.canvas.width = document.body.clientWidth;
        h = scope.canvas.height = document.body.clientHeight;
    }

    function on_click(e) {

        var ball;

        for(var i =0; i<scope.balls.length;++i){

            ball = scope.balls[i];

            if(ball.is_intersected(e.clientX, e.clientY)){

                ball.on_click();
                score += 10;
                safe_callback(options['on_score'], score)
            }
        }

    }


    function anim() {

        scope.update_all();
        scope.render();

        if(stopped){
            return;
        }

        requestAnimationFrame(anim)
    }

    var stopped = true;
    this.start = function () {

        stopped = false;
        anim();
    };


    this.stop = function () {

        stopped = true;
    };

    function clear() {

        ctx.clearRect(0,0,w,h );
    }

    this.balls = [];

    this.add_ball = function () {

        var ball = new Ball({

            x: -Math.random()*w,
            y: Math.random()*h,
            s_x: 0.9 + Math.random(),
            s_y: 0,
            r: Math.random()*360,
            r_s: (0.5 - Math.random()) * 7,

            image: rand_image(),
            part_images: options['part_images'],

            on_click: on_click
        });

        this.balls.push(ball);
    };


    function on_ball_remove() {

    }


    this.draw_all = function () {

        for(var i =0; i<this.balls.length;++i){

            this.balls[i].draw(ctx);
        }
    };

    this.update_all = function () {

        var ball;

        for(var i =0; i<this.balls.length;++i){

            ball = this.balls[i];
            ball.update();

            if(ball.x + ball.w > w ){

                on_leave_right()
            }
        }
    };


    function init() {

        for(var i = 0; i<options['count'];++i){
            scope.add_ball()
        }
    }


    var score = 0;

    function on_leave_right() {

        safe_callback(options['on_end'], score);
        scope.stop();
    }


    on_resize();
    init();
}


function Ball(options) {

    var scope = this;

    var s_x = options['s_x'],
        s_y = options['s_y'],
        r = options['r_s'],
        r_s = options['r_s'],
        image = options['image'];

    this.w = 0;
    this.h = 0;

    this.x = options['x'];
    this.y = options['y'];


    var img = document.createElement('img');
    img.onload = function () {

        loaded = true;
        scope.w = img.width;
        scope.h = img.height;
    };

    img.src = image;
    var loaded = false;


    this.draw = function (ctx) {

        draw(ctx);
        this.emitter.draw(ctx);
    };


    this.update = function () {

        move_x();
        move_y();
        rotate();
        this.emitter.update()
    };


    function move_x() {

        scope.x += s_x;
    }

    this.emitter = new Emitter({

        count: 10 + Math.random()*10,
        duration: 3000,
        images: options['part_images'],
        on_stop: on_emiter_stop
    });


    function move_y() {
        scope.y += s_y;
    }

    function rotate() {

        r += r_s;
    }


    function in_rad(val) {

        return val * Math.PI / 180;
    }

    var is_removed = false;

    function draw(ctx) {

        if(is_removed === true){
            return;
        }

        // ctx.save();
        //
        // ctx.translate(x+w/2, y+h/2);
        // ctx.rotate(in_rad(r));
        // ctx.translate(-x+w/2, -y+h/2);
        //
        ctx.drawImage(img, scope.x, scope.y, scope.w, scope.h);
        //
        // ctx.restore();
    }

    var intersected = false;

    this.is_intersected = function (m_x, m_y) {

        return (m_x >= scope.x  && m_x <=scope.x + scope.w ) && (m_y >= scope.y && m_y <= scope.y + scope.h);
    };


    this.on_click = function () {

        this.emitter.add_parts(scope.x, scope.y, 10);
        this.emitter.play();

        is_removed = true;
    };

    this.reinit = function () {

        is_removed = false;
        scope.x = -Math.random()*1000;

        // safe_callback(options['on_remove'], this);
    };

    function on_emiter_stop() {
        scope.reinit();
    }


}



function Emitter(options) {

    this.options = options;
    var scope = this;

    var images = options['images'];
    function rand_image() {
        var l = images.length-1;
        return images[Math.round(Math.random()*l)];
    }

    var parts = [];

    this.update = function () {

    };


    this.add_parts = function (x, y, count) {

        for(var i= 0; i<count;++i){
            this.add(x, y);
        }
    };


    this.add = function (x, y) {

        var part = new Part({

            x: x,
            y: y,

            s_x: Math.random() * 2,
            s_y: Math.random() * 2,

            image: rand_image(),
            time_leave: options['duration'],

            on_destroy: function (part) {
                scope.remove(part);
            }
        });

        parts.push(part);
        return part;
    };


    this.remove = function (part) {

        var i = parts.indexOf(part);
        parts.splice(i, 1);

        if(parts.length === 0){
            safe_callback(options['on_stop'], this)
        }
    };


    this.draw = function (ctx) {

        if(playing === false){
            return;
        }

        for(var i=0; i<parts.length; ++i){

            parts[i].draw(ctx);
        }
    };


    this.update = function () {

        if(playing === false){
            return;
        }

        for(var i=0; i<parts.length; ++i){

            parts[i].update();
        }
    };


    this.add_parts(options['count']);


    var playing = false;

    this.play = function () {


        for(var i=0; i<parts.length; ++i){
            parts[i].play();
        }

        playing = true;

    };
}



function Part(options) {

    var p_scope = this;
    var image = options['image'];
    var img = document.createElement('img');
    var w=0, h=0,
        x = options['x'],
        y = options['y'],
        s_x = options['s_x'],
        s_y = options['s_y'],
        time_leave = options['time_leave'];


    var time_start, playing = false;

    img.onload = function () {

        loaded = true;
        w = img.width;
        h = img.height;
    };
    img.src = image;
    var loaded = false;


    this.play = function () {

        time_start = +new Date();
        playing = true;
    };

    this.draw = function (ctx) {

        draw(ctx)
    };

    this.update = function () {

        var t_now = +new Date();

        if(t_now - time_start > time_leave){
            this.destroy();
        }

        update();
    };



    function update(){

        move_x();
        move_y();
    }


    function move_x() {

        x += s_x;
    }

    function move_y() {

        y += s_y;
    }


    this.destroy = function () {

        safe_callback(options['on_destroy'], this);
    };

    function draw(ctx) {

        ctx.drawImage(img, x, y);
    }


}



function safe_callback(cb, arg1, arg2) {

    if(cb !== undefined){
        cb(arg1, arg2)
    }
}












